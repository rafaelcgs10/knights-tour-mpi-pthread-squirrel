#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/time.h>
#include <mpi.h>
#include "desempatador.h"

#define MASTER 0


typedef struct {
	char visitado; // se ja foi visitado
	short int grau; //numero de adjacentes não visitados
	int num_passo;
} quadrado;

typedef struct 
{
	short int i, j;
	short int indice;
} vizinho;

typedef struct 
{
	int num_empatados;
	vizinho vizinhos_empatados[8];
}empatados;

int N, i_inicial;
int j_inicial;
int run, nucleos, run, offset, offset_interno;
pthread_t * threads;
char msg_resposta[10];
char msg_para;

int id_reso, size, taskid, flag;
int resposta;




void imprime_tabuleiro(int N, quadrado ** tabuleiro)
{
	//char nome[30];
	//sprintf(nome, "%d-tour.txt", id_reso);
	//FILE * out = fopen(nome, "w");
	int i, j;
	char *saida = (char*)malloc(sizeof(char)*300000);
	char aux[10];
	//int ultimo_passo = N*N + 1;
	//char aux[30] = tabuleiro[N][N];
	//sprintf(aux, "%d", ultimo_passo);

	//int max_width = strlen(aux) + 2;
	
	printf("%d %d %d\n", N, i_inicial, j_inicial);
	
	for(i = 0; i < N; i ++)
	{
		saida[0] = '\0';
		for(j = 0; j < N; j++)
		{
			//printf("%d\t", tabuleiro[i][j].num_passo + 1);
			sprintf(aux,"%d ",tabuleiro[i][j].num_passo-1);
			strcat(saida,aux);
			
		}
		printf("%s\n",saida);
		
	}
}

void seta_grau_quadrado(int N, quadrado ** tabuleiro, int i, int j)
{
	int x, y;
	tabuleiro[i][j].grau = 0;

	x = i - 2; y = j - 1;

	if(x >= 0 && y >=0 && tabuleiro[x][y].visitado == '0')
		tabuleiro[i][j].grau++;

	x = i - 1; y = j - 2;

	if(x >= 0 && y >=0 && tabuleiro[x][y].visitado == '0')
		tabuleiro[i][j].grau++;


	x = i + 1; y = j - 2;

	if(x < N && y >=0 && tabuleiro[x][y].visitado == '0')
		tabuleiro[i][j].grau++;

	x = i + 2; y = j - 1;

	if(x < N && y >=0 && tabuleiro[x][y].visitado == '0')
		tabuleiro[i][j].grau++;

	x = i + 2; y = j + 1;

	if(x < N && y < N && tabuleiro[x][y].visitado == '0')
		tabuleiro[i][j].grau++;

	x = i + 1; y = j + 2;

	if(x < N && y < N  && tabuleiro[x][y].visitado == '0')
		tabuleiro[i][j].grau++;

	x = i - 1; y = j + 2;

	if(x >= 0 && y < N && tabuleiro[x][y].visitado == '0')
		tabuleiro[i][j].grau++;

	x = i - 2; y = j + 1;

	if(x >= 0 && y < N && tabuleiro[x][y].visitado == '0')
		tabuleiro[i][j].grau++;
}

void inicializa(int N, quadrado ** tabuleiro)
{
	int i, j;

	for(i = 0; i < N; i++)
		for(j = 0; j < N; j++)
		{
			tabuleiro[i][j].num_passo = -1;
			tabuleiro[i][j].visitado = '0';
		}

	for(i = 0; i < N; i++)
		for(j = 0; j < N; j++)
		{
			seta_grau_quadrado(N, tabuleiro, i, j);
		}
}

void decrementa_grau_vizinho(int N, quadrado ** tabuleiro, int i, int j)
{
	int x, y;

	x = i - 2; y = j - 1;

	if(x >= 0 && y >=0 && tabuleiro[x][y].visitado == '0')
		tabuleiro[x][y].grau--;

	x = i - 1; y = j - 2;

	if(x >= 0 && y >=0 && tabuleiro[x][y].visitado == '0')
		tabuleiro[x][y].grau--;


	x = i + 1; y = j - 2;

	if(x < N && y >=0 && tabuleiro[x][y].visitado == '0')
		tabuleiro[x][y].grau--;

	x = i + 2; y = j - 1;

	if(x < N && y >=0 && tabuleiro[x][y].visitado == '0')
		tabuleiro[x][y].grau--;

	x = i + 2; y = j + 1;

	if(x < N && y < N && tabuleiro[x][y].visitado == '0')
		tabuleiro[x][y].grau--;

	x = i + 1; y = j + 2;

	if(x < N && y < N  && tabuleiro[x][y].visitado == '0')
		tabuleiro[x][y].grau--;

	x = i - 1; y = j + 2;

	if(x >= 0 && y < N && tabuleiro[x][y].visitado == '0')
		tabuleiro[x][y].grau--;

	x = i - 2; y = j + 1;

	if(x >= 0 && y < N && tabuleiro[x][y].visitado == '0')
		tabuleiro[x][y].grau--;
}

int encontra_menor_grau(int N, quadrado ** tabuleiro, int i, int j)
{
	int min_grau = 9;

	int x, y;

	x = i - 2; y = j - 1;

	if(x >= 0 && y >=0 && tabuleiro[x][y].visitado == '0' && tabuleiro[x][y].grau < min_grau)
		min_grau = tabuleiro[x][y].grau;

	x = i - 1; y = j - 2;

	if(x >= 0 && y >=0 && tabuleiro[x][y].visitado == '0' && tabuleiro[x][y].grau < min_grau)
		min_grau = tabuleiro[x][y].grau;

	x = i + 1; y = j - 2;

	if(x < N && y >=0 && tabuleiro[x][y].visitado == '0' && tabuleiro[x][y].grau < min_grau)
		min_grau = tabuleiro[x][y].grau;

	x = i + 2; y = j - 1;

	if(x < N && y >=0 && tabuleiro[x][y].visitado == '0' && tabuleiro[x][y].grau < min_grau)
		min_grau = tabuleiro[x][y].grau;

	x = i + 2; y = j + 1;

	if(x < N && y < N && tabuleiro[x][y].visitado == '0' && tabuleiro[x][y].grau < min_grau)
		min_grau = tabuleiro[x][y].grau;

	x = i + 1; y = j + 2;

	if(x < N && y < N && tabuleiro[x][y].visitado == '0' && tabuleiro[x][y].grau < min_grau)
		min_grau = tabuleiro[x][y].grau;

	x = i - 1; y = j + 2;

	if(x >= 0 && y < N && tabuleiro[x][y].visitado == '0' && tabuleiro[x][y].grau < min_grau)
		min_grau = tabuleiro[x][y].grau;

	x = i - 2; y = j + 1;

	if(x >= 0 && y < N && tabuleiro[x][y].visitado == '0' && tabuleiro[x][y].grau < min_grau)
		min_grau = tabuleiro[x][y].grau;

	return min_grau;
}

void encontra_empatados(int N, quadrado ** tabuleiro, int i, int j, int min_grau, empatados * candidatos_proximo)
{
	int num_empatados = 0;

	int x, y;

	y = j - 2;
	if(y >=0)
	{
		x = i - 1;
		if(x >= 0 && tabuleiro[x][y].visitado == '0' && tabuleiro[x][y].grau == min_grau)
		{
			candidatos_proximo->vizinhos_empatados[num_empatados].i = x;
			candidatos_proximo->vizinhos_empatados[num_empatados].j = y;
			candidatos_proximo->vizinhos_empatados[num_empatados].indice = 7;
			num_empatados++;
		}

		x = i + 1;

		if(x < N && tabuleiro[x][y].visitado == '0' && tabuleiro[x][y].grau == min_grau)
		{
			candidatos_proximo->vizinhos_empatados[num_empatados].i = x;
			candidatos_proximo->vizinhos_empatados[num_empatados].j = y;
			candidatos_proximo->vizinhos_empatados[num_empatados].indice = 6;
			num_empatados++;
		}
	}

	 y = j - 1;
	 if(y >=0)
	 {
		x = i + 2;
		if(x < N && tabuleiro[x][y].visitado == '0' && tabuleiro[x][y].grau == min_grau)
		{
			candidatos_proximo->vizinhos_empatados[num_empatados].i = x;
			candidatos_proximo->vizinhos_empatados[num_empatados].j = y;
			candidatos_proximo->vizinhos_empatados[num_empatados].indice = 5;
			num_empatados++;
		}


		x = i - 2;
		if(x >= 0 && tabuleiro[x][y].visitado == '0' && tabuleiro[x][y].grau == min_grau)
		{
			candidatos_proximo->vizinhos_empatados[num_empatados].i = x;
			candidatos_proximo->vizinhos_empatados[num_empatados].j = y;
			candidatos_proximo->vizinhos_empatados[num_empatados].indice = 8;
			num_empatados++;
		}
	}

	y = j + 2;
	if(y < N)
	{
		x = i + 1; 
		if(x < N && tabuleiro[x][y].visitado == '0' && tabuleiro[x][y].grau == min_grau)
		{
			candidatos_proximo->vizinhos_empatados[num_empatados].i = x;
			candidatos_proximo->vizinhos_empatados[num_empatados].j = y;
			candidatos_proximo->vizinhos_empatados[num_empatados].indice = 3;
			num_empatados++;
		}

		x = i - 1;
		if(x >= 0 && tabuleiro[x][y].visitado == '0' && tabuleiro[x][y].grau == min_grau)
		{
			candidatos_proximo->vizinhos_empatados[num_empatados].i = x;
			candidatos_proximo->vizinhos_empatados[num_empatados].j = y;
			candidatos_proximo->vizinhos_empatados[num_empatados].indice = 2;
			num_empatados++;
		}
	}

	y = j + 1;
	if(y < N)
	{
		x = i + 2; 
		if(x < N && tabuleiro[x][y].visitado == '0' && tabuleiro[x][y].grau == min_grau)
		{
			candidatos_proximo->vizinhos_empatados[num_empatados].i = x;
			candidatos_proximo->vizinhos_empatados[num_empatados].j = y;
			candidatos_proximo->vizinhos_empatados[num_empatados].indice = 4;
			num_empatados++;
		}

		x = i - 2;
		if(x >= 0 && tabuleiro[x][y].visitado == '0' && tabuleiro[x][y].grau == min_grau)
		{
			candidatos_proximo->vizinhos_empatados[num_empatados].i = x;
			candidatos_proximo->vizinhos_empatados[num_empatados].j = y;
			candidatos_proximo->vizinhos_empatados[num_empatados].indice = 1;
			num_empatados++;
		}
	}

	candidatos_proximo->num_empatados = num_empatados;
}

int desempata(empatados * candidatos_proximo, int desempatador[])
{
	int i, j, mais_primeiro = 0, distancia = 8;

	for(i = 0; i < candidatos_proximo->num_empatados; i++)
	{
		for(j = 0; j < 8; j++)
		{
			if(candidatos_proximo->vizinhos_empatados[i].indice == desempatador[j] && j < distancia)
			{
				distancia = j;
				mais_primeiro = i;
				break;
			}
		}
	}

	return mais_primeiro;
}

void warnsdorff(int N, quadrado ** tabuleiro, empatados * candidatos_proximo, int i_atual, int j_atual)
{
	int min_grau = encontra_menor_grau(N, tabuleiro, i_atual, j_atual);
	encontra_empatados(N, tabuleiro, i_atual, j_atual, min_grau, candidatos_proximo);

}


int encontra_proximo(int N, quadrado ** tabuleiro, int i_atual, int j_atual, int * i_proximo, int * j_proximo, int desempatador[])
{
	empatados candidatos_proximo;
	int proximo;
	warnsdorff(N, tabuleiro, &candidatos_proximo, i_atual, j_atual);

	if(candidatos_proximo.num_empatados > 0)
	{
		proximo = desempata(&candidatos_proximo, desempatador);
		*i_proximo = candidatos_proximo.vizinhos_empatados[proximo].i;
		*j_proximo = candidatos_proximo.vizinhos_empatados[proximo].j; 
		return 0;
	}

	return 1;
}

int visita_proximo(int N, quadrado ** tabuleiro, int * i_atual, int * j_atual, int desempatador[], int * cont_passo)
{
	int i_proximo, j_proximo;
	tabuleiro[*i_atual][*j_atual].visitado = '1';
	tabuleiro[*i_atual][*j_atual].num_passo = * cont_passo;
	int fim;
	decrementa_grau_vizinho(N, tabuleiro, *i_atual, *j_atual);

	fim = encontra_proximo(N, tabuleiro, *i_atual, *j_atual, &i_proximo, &j_proximo, desempatador);

	if(fim == 0)
	{
		*i_atual = i_proximo;
		*j_atual = j_proximo;
		return 0;
	}
	return 1;
}


int tour(int N, int i0, int j0, int desempatador[], quadrado ** tabuleiro)
{
	
	int i_atual = i0, j_atual = j0;
	int fim = 0;
	
	inicializa(N, tabuleiro);
	int cont_passo = 1;
	//int msg_para = 2;
	flag = 0;

	//sprintf(msg_para, "0");
	
	


/* IRecv ?*/

	while(fim == 0 && run != 0)
	{
		
		fim = visita_proximo(N, tabuleiro, &i_atual, &j_atual, desempatador, &cont_passo);
		cont_passo++;

	}


	if(cont_passo >= N * N)
	{
		id_reso++;
		//imprime_tabuleiro(N, tabuleiro);
		return 1;
	}

	return 0;
}

void tour_mestre(int N, int i0, int j0, int desempatador[])
{
	//quadrado tabuleiro[N][N];
	int i;
	quadrado ** tabuleiro = (quadrado **) malloc(N * sizeof(quadrado *));
	
	for(i = 0; i < N; i++)
		tabuleiro[i] = (quadrado *) malloc(N * sizeof(quadrado));
		
	int i_atual = i0, j_atual = j0;
	int fim = 0;
	inicializa(N, tabuleiro);
	int cont_passo = 1;
	//int msg_para = 2;
	flag = 0;


	while(fim == 0 && run != 0)
	{
		
		if (flag != 0)
		{
			{
				run = 0;
			}
			flag = -1;
		}
		
		fim = visita_proximo(N, tabuleiro, &i_atual, &j_atual, desempatador, &cont_passo);
		cont_passo++;

	}

	imprime_tabuleiro(N, tabuleiro);
	
	for(i = 0; i < N; i++)
		free(tabuleiro[i]);
		
	free(tabuleiro);
	
}

void escuta_rede(void * argp)
{
	MPI_Status status2;
	MPI_Request request2;
	flag = 0;
	MPI_Irecv(&msg_para, 1, MPI_INT, 0, 1, MPI_COMM_WORLD, &request2);
	
	while(run != 0)
	{
		MPI_Test(&request2, &flag, &status2);
		
		if(flag != 0)
		{
			run = 0;
			printf("Parou\n");
			break;
		}
	}	
}


void treco_paralelo(void * argp)
{
	int * start = (int *) argp;
	int i;
	
	quadrado ** tabuleiro = (quadrado **) malloc(N * sizeof(quadrado *));
	for(i = 0; i < N; i++)
		tabuleiro[i] = (quadrado *) malloc(N * sizeof(quadrado));


	for(i = *start; i < *start + offset_interno; i++)
	{
		if(run == 0)
		{
			//printf("parou thread do taskid = %d\n", taskid);
			break;
		}
		if(tour(N, i_inicial, j_inicial, desempatadores[i], tabuleiro) == 1)
		{
			/*
			printf("taskid = %d Encontrou um tour na tentativa #%d com desempatador ", taskid, i);
			for(j = 0; j < 8; j++)
				printf("%d", desempatadores[i][j]);

			printf("\n");*/

/* Enviar para todos processos */
/* MPI_ISend (ANY)*/
			
			//sprintf(msg_para, "1");
			//printf("Escreveu resposta = %d\n", i);
			sprintf(msg_resposta, "%d", i);

			run = 0;
			

			
/*	
			for(k = 1; k < size; k++)
				MPI_Isend(msg_para, 10, MPI_CHAR, k, 1, MPI_COMM_WORLD, &request);*/
		}
	}
	
	for(i = 0; i < N; i++)
		free(tabuleiro[i]);
		
	free(tabuleiro);
}

double wtime()
{
   struct timeval t;
   gettimeofday(&t, NULL);
   return t.tv_sec + t.tv_usec / 1000000.0;
}

void encontra_desempatador(int inicio)
{
	int i;
	id_reso = 0;
	
	threads = (pthread_t *) malloc((nucleos + 1) * sizeof(pthread_t));
	int starts[nucleos];
	//int msg_para = 2;
	//pthread_attr_t tattr;
	//size_t size_stack = N*N*sizeof(quadrado) + 100000;

	//pthread_attr_init(&tattr);

	//pthread_attr_setstacksize(&tattr, size_stack);
	

	

	for(i = 0; i < nucleos; i ++)
		starts[i] = inicio + (offset_interno * i);

	for(i = 0; i < nucleos; i++)
	{
		//printf("id = %d Criando uma thread com start = %d\n", taskid, starts[i]);
		pthread_create(&threads[i], NULL, (void *)treco_paralelo, (void *) &starts[i]);
	}
	
	pthread_create(&threads[nucleos], NULL, (void *)escuta_rede, NULL);
	
	for(i = 0; i < nucleos + 1; i++)
		pthread_join(threads[i], NULL);
		
	MPI_Send(msg_resposta, 10, MPI_CHAR, 0, 0, MPI_COMM_WORLD);

	free(threads);

}

int main(int argc, char * argv[])
{
	int i, tag = 0;
	MPI_Status status;
	MPI_Request request;
	int msg_para = 1;

	
	MPI_Init(&argc, &argv);

	MPI_Comm_rank(MPI_COMM_WORLD, &taskid);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	resposta = -1;
	
	char msg_inicio[10];

	int inicio = 0;
	
	if(argc < 4)
	{
		exit(0);
	}
	
	N = atoi(argv[1]), i_inicial = atoi(argv[2]), j_inicial = atoi(argv[3]);
	
	if(N < 100)
		nucleos = 1;
	else if(N >= 10000)
		nucleos = 3;
	else if(N >= 15000)
		nucleos = 1;
	else
		nucleos = 4;
		
	
	run = 1;

	offset = 40320 / (size - 1);
	
	//printf("Size = %d Offset = %d\n", size - 1, offset);

	offset_interno = offset / nucleos;

	if(taskid == 0)
	{
		//printf("Sou o meste\n");
		for(i = 1; i < size; i++)
		{
			sprintf(msg_inicio, "%d", (i -1) * offset);
			MPI_Send(msg_inicio, 10, MPI_CHAR, i, tag, MPI_COMM_WORLD);
		}
		//printf("Esperando resposta...\n");

		MPI_Recv(msg_resposta, 10, MPI_CHAR, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);
		
		//printf("Oi sou mestre, e recebi o desempatador = %s\n", msg_resposta);

		for(i = 1; i < size; i++)
		{
			if(status.MPI_SOURCE != i)
			{
				printf("parando %d\n", i);
				MPI_Isend(&msg_para, 1, MPI_INT, i, 1, MPI_COMM_WORLD, &request);
			}
		}
		
		tour_mestre(N, i_inicial, j_inicial, desempatadores[atoi(msg_resposta)]);
		
/*esperar a confirmacao de recebimento dos processos */
		
/* esperar a resposta correta */

	}
	else
	{
		MPI_Recv(msg_inicio, 10, MPI_CHAR, 0, 0, MPI_COMM_WORLD, &status);
		//printf("Sou um escravo com start: %s\n", msg_inicio);
		inicio = atoi(msg_inicio);		
		encontra_desempatador(inicio);
			
/* enviar somente se achou algo util */
	}
	
	//MPI_Barrier(MPI_COMM_WORLD);

	MPI_Finalize();

	return 0;
}
