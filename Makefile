CFLAGS = -Wall -pthread -O2 
SOURCES = main.c
CC = mpicc

all: main

main.o: main.c
	$(CC) -c main.c $(CFLAGS)

main: main.o
	$(CC) main.o -o Tour $(CFLAGS)

clean:
	rm -f Tour main.o
